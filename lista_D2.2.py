# Autora:"Martha Cango"
# Email:"martha.cango@unl.edu.ec"
# Read a 2D list of integers:
m, n = [int(j) for j in input().split()]
b = [[int(j) for j in input().split()] for i in range(m)]
# Print a value:
# print(a)
maximo_total, maximo_i, maximo_j = b[0][0], 0, 0
for i in range(m):
    for j in range(n):
        if b[i][j] > maximo_total:
            maximo_total, maximo_i, maximo_j = b[i][j], i, j
print(maximo_i, maximo_j)
