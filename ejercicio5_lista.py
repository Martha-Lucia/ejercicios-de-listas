# Autora:"Martha Cango"
# Email:"martha.cango@unl.edu.ec"
# Dada una lista de números, determine e imprima el número
# de elementos que son mayores que sus dos vecinos.
# El primero y el último elemento de la lista no deben
# considerarse porque no tienen dos vecinos.
# Lea una lista de enteros:
a = [int(s) for s in input().split()]
# Print a value:
# print(a)
cont = 0
for s in range(1, len(a)-1):
    if a[s-1] < a[s] and a[s] > a[s+1]:
        cont = cont + 1
print(cont)
